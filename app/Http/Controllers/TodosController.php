<?php

namespace todoApp\Http\Controllers;

use Illuminate\Http\Request;

use todoApp\Http\Requests;
use todoApp\Http\Controllers\Controller;
use todoApp\App\Todo;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $todos = Todo::add();
        return $todos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $todo = Todo::create(Request::all());
        return $todo;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $todo = Todo::find($id);
        $todo->done = Request::input('done');
        $todo->save();

        return $todo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Todo::destroy($id);
    }
}
